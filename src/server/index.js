
var path = require('path')
const express = require('express')
const mockAPIResponse = require('./mockAPI.js')
const dotenv = require('dotenv')
const cors = require('cors');
const bodyParser = require('body-parser');
const request = require('request');
dotenv.config()
console.log("proc", process.env.API_ID)
console.log("proc", process.env.pixabayKey)
const app = express()
app.use(cors())
app.use(bodyParser.json())  
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(express.static('dist'))
console.log(__dirname)
app.get('/', function(req, res){
    res.sendFile(path.resolve('dist/index.html'));   
});
// designates what port the app will listen to for incoming requests
app.listen(3001, function () {
    console.log('Example app listening on port 3001!')
})
app.get('/test', function (req, res) {
    res.send(mockAPIResponse)
})
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*'); // will try to configure a proxy for darkWebApi, CODE ATTRIBUTED TO UDACITY LINK FOR CREATING PROXY
    next();
  });

app.post('/getWeather',(req,res) => {   // go through local host to hide api key
  console.log("request body is", req.body)
  console.log(req.body.url_part2)
  let url = `${req.body.url_part1}${process.env.darkKey}/${req.body.url_part2}`
  console.log("URL IS", url)
  request(
    { url: url },
    (error, response, body) => {
      if (error || response.statusCode !== 200) {
        return res.status(500).json({ type: 'error', message: error.message });
      }
      res.json(JSON.parse(body));
    }
  )

});
app.post('/getImage',(req,res) => {    // go through local host to hide api key
  let x = `https://pixabay.com/api/?key=${process.env.pixabayKey}&q=${req.body.city}`
  console.log(x)
  request(
    { url: x },
    (error, response, body) => {
      if (error || response.statusCode !== 200) {
        return res.status(500).json({ type: 'error', message: error.message });
      }
      res.json(JSON.parse(body));
    }
  )
});
module.exports = app;
  
  






