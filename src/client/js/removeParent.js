

function removeParent(){ // finds the nearest trip container in our html and deletes it
    function closestByID(el, clazz) {  // I looked this up on stack overflow but changed the condition to do a specific lookup based on button parameters
        while (!el.id.includes(clazz) || el.id.includes("trip")) { // we want to delete div with certain name, but that name is also in our button...
                                                                    // since 'trip' is only in our button name, this skips buttons for deletion
            el = el.parentNode;
            if (!el) {
                return null;
            }
        }
        return el;
    }
    var check = closestByID(this, "contDiv")
    check.parentNode.removeChild(check)
    console.log("this is", this)
    var temp = this.id.split("_")
    var sel = temp[1]
    console.log("sel is", sel)
    var element = document.querySelectorAll(`a[href=${sel}`) // get all the links that match this buttons parameters to delete the right ones
    Array.prototype.forEach.call( element, function( node ) {
        node.parentNode.removeChild( node );
    });
}
export {removeParent}


