/* Global Variables */

import './styles/resets.scss'
import './styles/base.scss'
import './styles/form.scss'
import './styles/footer.scss'
import './styles/header.scss'
import { removeParent } from './js/removeParent.js'


let myList = document.querySelector("#navbar__list");
let darkUrl = 'https://api.darksky.net/forecast/'
document.getElementById('ArrivalDate').valueAsDate = new Date();

let globalCount = 0 // change button input text if adding multiple trips




const retrieveData = async (city)=>{ // calling geonames api
    let u = `http://api.geonames.org/searchJSON?q=${city}&maxRows=10&username=mpesek4`
    console.log("url is", u)
    const request = await fetch(u);
    try{
        const allData = await request.json();
        console.log("all data is", allData);
        return allData
    }
    catch(error) {
        console.log("error", error);
    }
}

const createDisplay = async (data,arrivalTime) => { // loop through however many maps we need to call weather api for that location and handle posting
    console.log("does data pass to createDisplay?", data)
    console.log("arrival time is", arrivalTime)
    let i = 0
    for ( const element of data.geonames){
        i = i + 1
        console.log(i)
        //console.log(element.toponymName)
        await popDisplay(element)
        
    }
    let fields = document.getElementsByTagName('p')
    console.log("fields are", fields)

    for(let i = 0; i <fields.length;i++){ // create event listeners for all the search options for potential cities, user will select one to create trip
        let item = fields[i]
        console.log("item is", item)
        let num = item.id
        item.addEventListener("click", async () =>{
            var modal = document.getElementById("possCities");
            modal.style.display = "none";
            let city = item.innerHTML
            var div = document.getElementById('possCities')
            while(div.firstChild){
                div.removeChild(div.firstChild);
            }
            console.log("done")
            await getWeather(data,arrivalTime)
            .then(function(data){
                pixaBayStuff(data,city,arrivalTime)
            })
        })       
    }
    return data
}

const popDisplay = async (element) => { // possible cities are put into a modal in element possCities
    var city = document.createElement('p')
    city.setAttribute("class","modal-content")
    city.innerHTML = element.toponymName

    let possCities = document.getElementById("possCities")
    possCities.appendChild(city)

}
const getWeather = async (data,arrivalTime) => { // loop through however many maps we need to call weather api for that location and handle posting

    var dateEntered = new Date(arrivalTime)
    console.log("date entered", dateEntered)
    let ms = dateEntered.getTime()
    ms = Math.round(ms/1000)
    console.log("time is",ms)
    let lat = data.geonames[0].lat
    let lon = data.geonames[0].lng
    let url_part1 = darkUrl
    let url_part2 =  lat +"," + lon+"," + ms
    let myUrl = {'url_part1': url_part1,'url_part2':url_part2}

    console.log("type of data is", typeof myUrl)

    const response = await fetch('http://localhost:3001/getWeather', {
        method: 'POST', 
        credentials: 'same-origin', 
        headers: {
            'Content-Type': 'application/json',
        },     
        body: JSON.stringify(myUrl),   
    });
    try {
        const newData = await response.json()
        console.log("new data is", newData)
        return newData
    }catch(error) {
    console.log("error", error)
    }
}
const pixaBayStuff = async (data,city,arrivalTime) => { // This function is our last api call so I added the UI updates after the call in the try
    let cityBody = {'city': city}
    const request = await fetch('http://localhost:3001/getImage', {
        method: 'POST', 
        credentials: 'same-origin', 
        headers: {
            'Content-Type': 'application/json',
        },     
        body: JSON.stringify(cityBody),   
    });
    
    try{
        const allData = await request.json()
        console.log("pixabay data is", allData)  
        globalCount = globalCount + 1
        var containerDiv = document.createElement("div")
        containerDiv.setAttribute("id","contDiv"+globalCount)
        containerDiv.setAttribute("class","contDiv")
        var link = document.createElement("a")
        link.setAttribute("id","link")  // creating links so user can scroll to their trip from header ala project 2
        link.href = "contDiv"+globalCount
        link.text = "Trip to: "+ city
        myList.appendChild(link)
        const currentCount = globalCount

        link.addEventListener("click", function(event){ // event handler that removes default of link behavior and replaces with a smooth scroll
            event.preventDefault();
            let element = document.getElementById("contDiv"+currentCount)
            element.scrollIntoView({behavior: "smooth"});
          });
        let imageHolder = document.createElement("div") // the rest of this block of code is creating Div holders and appending the right children to display
        imageHolder.setAttribute("class","box")
        imageHolder.setAttribute("id","imageHolder")

        let outerImageHolder = document.createElement("div")
        outerImageHolder.setAttribute("class","box")
        outerImageHolder.setAttribute("id","outerImageHolder")
        containerDiv.appendChild(outerImageHolder)

        var tripInfo = document.createElement("div")
        tripInfo.setAttribute("id","tripInfo")
        tripInfo.setAttribute("class","box")
        document.getElementById("holder zip").appendChild(containerDiv)

        var tripContainer = document.createElement("div")
        tripContainer.setAttribute("id","tripContainer")
        tripContainer.setAttribute("class","box")
        containerDiv.appendChild(tripContainer)

        var tripIntro = document.createElement("div")
        tripIntro.setAttribute("id","tripIntro")
        tripIntro.setAttribute("class","box")
        var text1 = document.createElement("p")
        text1.innerHTML = `Your Trip to ${city} <br> Departing: ${arrivalTime}`
        tripIntro.appendChild(text1)     
        var text2 = document.createElement("p")
        tripInfo.appendChild(text2)
       
        console.log("LAST DATA IS", data)
        text2.innerHTML = `Weather Forecast: ${data.daily.data[0].summary}<br><br> High: ${data.daily.data[0].temperatureHigh}'C Low: ${data.daily.data[0].temperatureLow}'C `
        let cityImage = document.createElement("img")
        cityImage.setAttribute("alt","no image yet")
        if(allData.hits[0]!=null){
            cityImage.setAttribute("src",allData.hits[0].largeImageURL)
        }
        
        cityImage.setAttribute("id","tripImage")
        console.log("city image is",cityImage)
        console.log("arrival time is", typeof arrivalTime)
       
        const currDate = new Date()
        let countDown = 1+Math.floor(Date.parse(arrivalTime)-currDate); // calculates remaining time from arrival to current date
        const oneCheck = 1+Math.floor(countDown / (60*60*24*1000)) 
        let timeRemaining = 0
        if(oneCheck ===  1) { timeRemaining =  1+Math.floor(countDown / (60*60*24*1000)) + ' day until your departure'}
        else{ timeRemaining =  1+Math.floor(countDown / (60*60*24*1000)) + ' days until your departure'}

        console.log("countdown is" ,timeRemaining)
        
        var timeRem = document.createElement("div")
        timeRem.setAttribute("id","tripTimeRem")
        timeRem.setAttribute("class","box")
        
        var text3 = document.createElement("p")
        timeRem.appendChild(text3)
        text3.innerHTML = timeRemaining
        
        var button = document.createElement('button');
        const cc = globalCount
        button.setAttribute("id","tripButton_contDiv"+cc)
        button.setAttribute("class","box")
        button.innerHTML = 'REMOVE TRIP'
        button.addEventListener("click",removeParent)
        
        
        tripContainer.appendChild(tripIntro)
        tripContainer.appendChild(button)
        tripContainer.appendChild(tripInfo)
        outerImageHolder.appendChild(imageHolder)  
        tripContainer.appendChild(timeRem)
        imageHolder.appendChild(cityImage)
        


    }catch(error){
        console.log("error", error)
    }
}
document.getElementById("generate").addEventListener("click", () => { // event listener for our main add a trip button
    var modal = document.getElementById("possCities");
    modal.style.display = "block";

    let city = document.getElementById("city").value
    let arrivalTime = document.getElementById("ArrivalDate").value
    document.getElementById('ArrivalDate').valueAsDate = new Date();
    document.getElementById('city').value = ''
    document.getElementById('city').placeholder = 'enter another city'
    
    retrieveData(city)
    .then(function(data){
        createDisplay(data,arrivalTime)
    })
})
    


