# Dependancies

"devDependencies": {
    "@babel/core": "^7.8.3",
    "@babel/preset-env": "^7.8.3",
    "babel-loader": "^8.0.6",
    "clean-webpack-plugin": "^3.0.0",
    "css-loader": "^3.4.2",
    "eslint": "^6.8.0",
    "eslint-config-prettier": "^6.10.0",
    "eslint-plugin-prettier": "^3.1.2",
    "html-webpack-plugin": "^3.2.0",
    "jest": "^24.9.0",
    "node-sass": "^4.13.1",
    "prettier": "^1.19.1",
    "sass-loader": "^8.0.2",
    "style-loader": "^1.1.2",
    "webpack-dev-server": "^3.10.1",
    "workbox-webpack-plugin": "^4.3.1"

# About My Travelog

  This app allows a user to input a city and a date for a trip. A modal will popup with potential cities similar to the name the user inputted, and upon
  selection will traverse multiple API endpoints to return a picture of that city and a weather forecast.

  Dynamically created links in the header will allow a user to navigate the page to multiple trips they create, and removing a trip will delete these links.
  The app has responsive design and uses service workers for offline functionality.
